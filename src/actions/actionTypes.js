export const GET_USER = 'GET_USER';

export const SET_ARTICLES = 'SET_ARTICLES';

export const SET_MODAL_TYPE = 'SET_MODAL_TYPE';

export const SIGN_IN = 'SIGN_IN';

export const SIGN_IN_ERROR = 'SIGN_IN_ERROR';

export const SET_TEMP_FORMDATA = 'SET_TEMP_FORMDATA';

export const SET_CURRENT_USER = 'SET_CURRENT_USER';

export const AUTHENTICATED = 'authenticated_user';
export const UNAUTHENTICATED = 'unauthenticated_user';
export const AUTHENTICATION_ERROR = 'authentication_error';

