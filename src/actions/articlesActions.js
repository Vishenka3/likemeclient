import { SET_ARTICLES } from '../actions/actionTypes';

export const fetchArticles = (articlesArray) => dispatch => {
    dispatch({
        type: SET_ARTICLES,
        payload: articlesArray
    })
};