import axiosInstance from '../api/axiosWrapper';
import Modal from '../containers/Modal/Modal';

import { AUTHENTICATED, AUTHENTICATION_ERROR, SET_CURRENT_USER } from '../actions/actionTypes';

const URL = 'http://localhost:4000';

export const signInAction = (registrationUser) => {
    return async dispatch => {
        try {
            //console.log(registrationUser);
            const res = await axiosInstance.post(`${URL}/signin`, registrationUser);

            dispatch({
                type: SET_CURRENT_USER,
                payload: res.data.user
            });

            dispatch({type: AUTHENTICATED});
            console.log(res.data);
            localStorage.setItem('accessToken', res.data.token);
            if(res.data.token){
                Modal.hideModal();
            }
        } catch (error) {
            dispatch({
                type: AUTHENTICATION_ERROR,
                payload: {error}
            });
        }
    }
};

export const logInAction = (loginUser) => {
    return async dispatch => {
        try {
            console.log(loginUser);
            const res = await axiosInstance.post(`${URL}/login`, loginUser);

            dispatch({
                type: SET_CURRENT_USER,
                payload: res.data.user
            });

            dispatch({type: AUTHENTICATED});
            console.log('asd: ' + res);

            localStorage.setItem('accessToken', res.data.token);
            if(res.data.token){
                Modal.hideModal();
            }

            //history.push('/secret');
        } catch (error) {
            dispatch({
                type: AUTHENTICATION_ERROR,
                payload: {error}
            });
        }
    }
};

export const autoLogin = () => dispatch => {
    dispatch({type: AUTHENTICATED});
};
