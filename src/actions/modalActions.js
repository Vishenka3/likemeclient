import { SET_MODAL_TYPE } from '../actions/actionTypes';

export const setModalType = (modalType) => dispatch => {
    dispatch({
        type: SET_MODAL_TYPE,
        payload: {modalType}
    })
};