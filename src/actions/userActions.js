export const getUser = (id) => dispatch => {
    dispatch({
        type: 'GET_USER',
        payload: {id}
    })
};

export const setCurrentUser = (user) => dispatch => {
    dispatch({
        type: 'SET_CURRENT_USER',
        payload: user
    })
};