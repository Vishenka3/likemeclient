import React, { Component } from 'react';
import { connect } from 'react-redux';
import axiosInstance from '../../api/axiosWrapper';

import { fetchArticles } from "../../actions/articlesActions";
import { setCurrentUser } from "../../actions/userActions";
import { autoLogin } from "../../actions/authorizationActions";

import { isAuth } from "../../selectors/selectors";

import ProfileHeader from '../ProfileHeader/ProfileHeader';
import AuthHeader from '../AuthHeader/AuthHeader';
import Modal from '../Modal/Modal';
import MainFeed from '../MainFeed/MainFeed';

import logo_like from '../../resources/logo-like.jpg'

const mapDispatchToProps = {
    setArticles: fetchArticles,
    setCurrentUser: setCurrentUser,
    autoLogin: autoLogin
};

const mapStateToProps = state => ({
    isAuth: isAuth(state)
});

class App extends Component {
    componentWillMount(){
        axiosInstance.get('http://localhost:4000/articles')
            .then( res => {
                console.log(res.data);
                this.props.setArticles(res.data);
            })
            .catch( error => {
                console.log(error);
            });

        if(localStorage.getItem("accessToken")){
            axiosInstance.post('http://localhost:4000/hello')
                .then( res => {
                    console.log(res.data);
                    this.props.setCurrentUser(res.data);
                    this.props.autoLogin();
                })
                .catch( error => {
                    console.log(error);
                })
        }
    }


    render () {
        return (
            <div id="app" className="app">
                <Modal/>
                <header>
                    <div className="logo" id="logo">
                        <img src={logo_like} alt="" className="logo-like"/>
                        <h2>Me</h2>
                    </div>
                    <div className="page-name">
                        <h1>Like Me</h1>
                    </div>
                    {this.props.isAuth ? <ProfileHeader/> : <AuthHeader/>}
                </header>
                <main id="main">
                    <MainFeed/>
                    <aside/>
                </main>
                <footer></footer >
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);