import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setModalType } from '../../actions/modalActions'
import { getModalType } from '../../selectors/selectors'

import Modal from '../Modal/Modal';

const mapStateToProps = state => ({
    modalType: getModalType(state)
});

const mapDispatchToProps = {
    setModalType: setModalType
};

class AuthHeader extends Component {
    componentDidMount(){
        this.buttons = document.getElementsByClassName('button');
        this.overlay = document.getElementById('overlay');
        this.modal = document.getElementById('modal-window');

        for(let i=0; i<this.buttons.length; i++){
            this.buttons[i].addEventListener('click', (event) => {
                this.props.setModalType(event.target.id === 'log-header' ? 'authorization' : 'registration');
                console.log(this.props.modalType);
                Modal.showModal();
            });
        }
    }

    render () {
        return (
            <div className="authHeader" id="authHeader">
                <div className="button button-sign-header" id="sign-header">Sign in</div>
                <div className="button button-log-header" id="log-header">Log in</div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthHeader);