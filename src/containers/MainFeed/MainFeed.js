import React, { Component } from 'react';
import { connect } from 'react-redux';
import axiosInstance from '../../api/axiosWrapper';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { faHeart as heart_regular } from '@fortawesome/free-regular-svg-icons'

import { isAuth, getArticles } from '../../selectors/selectors';

const mapStateToProps = state => ({
    isAuth: isAuth(state),
    articles: getArticles(state),
});

class MainFeed extends Component {
    handleClick = event => {
        console.log('click');


        axiosInstance.post('http://localhost:4000/like')
            .then( res => {
                console.log(res.data);
                this.props.setCurrentUser(res.data);
                this.props.autoLogin();
            })
            .catch( error => {
                console.log(error);
            })
    };

    render () {
        return (
            <div className="main-feed" id="main-feed">
                <h3 className="feed-intro">hello</h3>
                {this.props.articles.map( (item, index) => {
                    return (
                        <article className="feed-article" id={`article_#${index}`}>
                            <h4 className="article-name">{item.name}</h4>
                            <span className="author_name">{item.author_name}</span>
                            <div className="article-text">{item.text}</div>
                            <div className="meta">
                                <span  className="article-date">{`${item.createdAt.substring(0, 10)} ${item.createdAt.substring(12, 16)}`}</span>
                                <div onClick={this.handleClick}>
                                    <FontAwesomeIcon icon={heart_regular}/>
                                    <span>{item.like_number}</span>
                                </div>
                            </div>
                        </article>
                    )
                })}
            </div>
        )
    }
}

export default connect(mapStateToProps)(MainFeed);