import React, { Component } from 'react';
import { connect } from 'react-redux';

import Registration from '../Registration/Registration';
import Authorization from '../Registration/Authorization';
import NewArticle from '../NewArticle/NewArticle';

import { setModalType } from '../../actions/modalActions'
import { getModalType } from "../../selectors/selectors";

const mapStateToProps = state => ({
    modalType: getModalType(state)
});

const mapDispatchToProps = {
    setModalType: setModalType
};

class Modal extends Component {
    static hideModal(){
        const overlay = document.getElementById('overlay');
        const modal = document.getElementById('modal-window');
        modal.style.opacity = 0;
        overlay.style.opacity = 0;
        setTimeout( () => {
            modal.style.display = 'none';
            overlay.style.display = 'none';
        }, 500)
    }

    static showModal(){
        const overlay = document.getElementById('overlay');
        const modal = document.getElementById('modal-window');
        overlay.style.display = 'flex';
        modal.style.display = 'block';
        setTimeout( () => {
            modal.style.opacity = 1;
            overlay.style.opacity = 1;
        },2);
    }

    componentDidMount(){
        this.overlay = document.getElementById('overlay');
        this.overlay.addEventListener('click', (event) => {
            if(event.target.id !== 'overlay'){
                event.preventDefault();
            }else{
                Modal.hideModal()
            }
        });
    }

    render () {
        return (
            <div className="modal" id="modal">
                <div className="overlay" id="overlay">
                    <div className="modal-window" id="modal-window">
                        {this.props.modalType === 'registration' ? <Registration/> : this.props.modalType === 'newArticle' ? <NewArticle/> : <Authorization/>}
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal);