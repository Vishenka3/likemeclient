import React, { Component } from 'react';
import { connect } from 'react-redux';
import axiosInstance from '../../api/axiosWrapper';

import { setModalType } from '../../actions/modalActions';
import { logInAction } from '../../actions/authorizationActions';
import { isAuth } from '../../selectors/selectors';
import { fetchArticles } from "../../actions/articlesActions";

const mapDispatchToProps = {
    articles: fetchArticles,
    setModalType: setModalType,
    logInAction: logInAction,
};

const mapStateToProps = state => ({
    isAuth: isAuth(state)
});

class NewArticle extends Component {
    componentDidMount(){
        this.tempArticle = {};
    }

    handleChange = (event) => {
        this.tempArticle[event.target.name] = event.target.value;
    };

    handleSubmit = () => {
        axiosInstance.post('http://localhost:4000/new-article', this.tempArticle)
            .then( res => {
                console.log(res.data);
                axiosInstance.get('http://localhost:4000/articles')
                    .then( res => {
                        console.log(res.data);
                        this.props.articles(res.data);
                    })
                    .catch( error => {
                        console.log(error);
                    })
            })
            .catch( error => {
                console.log(error);
            })
        //this.props.logInAction(this.tempUser);
        //console.log('isAuth: ' + this.props.isAuth);
    };

    render () {
        return (
            <div className="registration-window" id="registration-window">
                <h3 className="reg_intro">Add new Article</h3>
                <form action="#" className="article-form" id="article-form">

                    <div className="newArticle-wrapper">
                        <label htmlFor="topic">Re: </label>
                        <input type="text" name="topic" id="topic" className="reg-input" placeholder="Topic" onChange={this.handleChange} autoFocus/>
                    </div>
                    <textarea name="text" id="text" className="reg-input" placeholder="Your Text" onChange={this.handleChange}/>

                    <div className="newArticle-wrapper">
                        <input type="submit" onClick={this.handleSubmit} name="publish-article" value="publish" />
                        <a href="#" onClick={this.handleSubmit} name="close-window" id="close-window">Cancel</a>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewArticle);