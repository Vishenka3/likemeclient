import React, { Component } from 'react';
import { connect } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons'

import { setModalType } from '../../actions/modalActions'
import { getModalType, getCurrentUserName } from '../../selectors/selectors'
import Modal from "../Modal/Modal";

const mapStateToProps = state => ({
    modalType: getModalType(state),
    currentUserName: getCurrentUserName(state)
});

const mapDispatchToProps = {
    setModalType: setModalType
};

class HeaderProfile extends Component {
    componentDidMount(){
        this.addNew = document.getElementById('add_new_article');

        this.addNew.addEventListener('click', () => {
            this.props.setModalType('newArticle');
            Modal.showModal();
            /*const window = document.getElementById('modal-window');
            window.style.minWidth = '20%';*/
        });
    }

    render () {
        return (
            <div className="header-profile" id="header-profile">
                <div className="icon-svg-wrapper" id="add_new_article">
                    <p>New</p>
                    <FontAwesomeIcon className="icon-svg" icon={faPlus} />
                </div>
                <h2 className="fullname">{this.props.currentUserName}</h2>
                <div className="profile-logo"/>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderProfile);

