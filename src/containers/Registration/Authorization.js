import React, { Component } from 'react';
import { connect } from 'react-redux';
//import axios from 'axios';

import { setModalType } from '../../actions/modalActions';
import { logInAction } from '../../actions/authorizationActions';
import { isAuth } from '../../selectors/selectors';

const mapDispatchToProps = {
    setModalType: setModalType,
    logInAction: logInAction,
};

const mapStateToProps = state => ({
    isAuth: isAuth(state)
});

class Authorization extends Component {
    componentDidMount(){
        this.overlay = document.getElementById('overlay');
        this.modal = document.getElementById('modal-window');
        this.tempUser = {};
    }

    handleChange = (event) => {
        this.tempUser[event.target.name] = event.target.value;
    };

    handleSubmit = () => {
        this.props.logInAction(this.tempUser);
        //console.log('isAuth: ' + this.props.isAuth);
    };

    render () {
        return (
            <div className="registration-window" id="registration-window">
                <h3 className="reg_intro">Sign in</h3>
                <form action="#" className="registration-form" id="registration-form">

                    <input type="text" name="regUsername" id="username" className="reg-input" placeholder="Username" onChange={this.handleChange} autoFocus/>
                    <input type="password" name="regPassword" id="password" className="reg-input" placeholder="Password" onChange={this.handleChange}/>

                    <input type="submit" onClick={this.handleSubmit} name="signup_submit" value="Sign me up" />
                </form>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);