import React, { Component } from 'react';
import { connect } from 'react-redux';
//import axios from 'axios';

import { setModalType } from '../../actions/modalActions';
import { signInAction } from '../../actions/authorizationActions';

const mapDispatchToProps = {
    setModalType: setModalType,
    signInAction: signInAction,
};

class Registration extends Component {
    componentDidMount(){
        this.tempUser = {};
    }

    handleChange = (event) => {
        this.tempUser[event.target.name] = event.target.value;
    };

    handleSubmit = () => {
        //console.log('submit reg');

        //event.preventDefault();
        this.props.signInAction(this.tempUser);

        //axios.post('http://localhost:4000/register', this.tempUser)
    };

    render () {
        return (
            <div className="registration-window" id="registration-window">
                <h3 className="reg_intro">Sign in</h3>
                <form action="#" className="registration-form" id="registration-form">

                    <input type="text" name="regUsername" id="reg-username" className="reg-input" placeholder="Username" onChange={this.handleChange}/>
                    <input type="password" name="regPassword" id="reg-password" className="reg-input" placeholder="Password" onChange={this.handleChange}/>
                    <input type="password" name="regRetypePassword" id="reg-retype-password" className="reg-input" placeholder="Retype password" onChange={this.handleChange}/>
                    <input type="text" name="regFirstname" id="reg-firstname" className="reg-input" placeholder="First Name" onChange={this.handleChange}/>
                    <input type="text" name="regLastname" id="reg-lastname" className="reg-input" placeholder="Last Name" onChange={this.handleChange}/>
                    <input type="email" name="regEmail" id="reg-email" className="reg-input" placeholder="E-mail" onChange={this.handleChange}/>

                    <input type="submit" onClick={this.handleSubmit} name="signup_submit" value="Sign me up" />
                </form>
            </div>
        )
    }
}

export default connect(null, mapDispatchToProps)(Registration);