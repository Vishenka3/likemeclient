import { AUTHENTICATED, AUTHENTICATION_ERROR, UNAUTHENTICATED } from '../actions/actionTypes';

export default (state = {}, action) => {  //eslint ругается на intend??
    switch(action.type) {
    case AUTHENTICATED:
        return {
            authenticated: true
        };
    case UNAUTHENTICATED:
        return {
            authenticated: false
        };
    case AUTHENTICATION_ERROR:
        return {
            error: action.payload
        };
    default:
        return state
    }
}