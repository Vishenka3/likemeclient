import initialState from './initial.state';

import { SET_MODAL_TYPE } from '../actions/actionTypes';

export default (state = initialState, action) => {  //eslint ругается на intend??
    switch(action.type) {
    case SET_MODAL_TYPE:
        return {
            //...state,
            modalType: action.payload.modalType,
        };
    default:
        return state
    }
}