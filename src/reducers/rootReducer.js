import {combineReducers} from 'redux';

import articles from './articlesReducer';
import modals from './modalReducers';
import authReducer from './authorizationReducers';
import currentUser from './userReducers';

const rootReducer = combineReducers({
    articles,
    modals,
    authReducer,
    currentUser
});

export default rootReducer;