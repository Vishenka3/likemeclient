import { SET_CURRENT_USER } from '../actions/actionTypes';

export default (state = {currentUser:{}}, action) => {
    switch(action.type) {
    case SET_CURRENT_USER:
        return {
            currentUser: action.payload,
        };
    default:
        return state
    }
}