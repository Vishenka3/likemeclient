export const getModalType = state => state.modals.modalType;

export const isAuth = state => state.authReducer.authenticated;

export const getCurrentUserName = state => `${state.currentUser.currentUser.first_name} ${state.currentUser.currentUser.last_name}`;

export const getArticles = state => {
    return state.articles
};

