const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin  = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");


const parentDir = path.join(__dirname, '../src');

module.exports = {
    mode: "development",
    entry:  [
        'babel-polyfill',
        path.join(parentDir, 'index.js'),
        path.join(parentDir, 'index.scss'),
    ],
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.(js|jsx)?$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.js?$/,
                use: ['babel-loader', 'eslint-loader'],
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader", "sass-loader"]

                })
            },
            {
                test: /\.(jpe?g|png|gif|ico|woff(2)?|ttf|eot)$/i,
                use: 'file-loader',
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new ExtractTextPlugin({
            filename: 'index.css',
            disable: false,
            allChunks: true,
        }),
    ],
    output: {
        path: parentDir + '/bundle',
        filename: 'bundle.js'
    },
    devServer: {
        port: 3000,
        contentBase: parentDir,
        historyApiFallback: true,
    }
};